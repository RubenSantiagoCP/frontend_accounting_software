import { City } from './City'
import { Country } from './Country';


export interface Location {
  id?: number;
  address: string;
  city: number;
  country: number;
  department: number;
}
